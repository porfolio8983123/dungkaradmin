import { useState } from "react";
import { Routes, Route, useLocation,Navigate } from "react-router-dom";
import Sidebar from "./scenes/global/Sidebar";
import LoginPage from "./scenes/Login";
import Dashboard from "./scenes/dashboard";
import Booking from "./scenes/booking";
import News from "./scenes/news";
import Completed from "./scenes/completed";
import Approved from "./scenes/approved";
import Cancelled from "./scenes/cancelled";
import Records from "./scenes/records";
import Profile from "./scenes/profile";
import { CssBaseline, ThemeProvider } from "@mui/material";
import { ColorModeContext, useMode } from "./theme";

function App() {
  const [theme, colorMode] = useMode();
  const [isSidebar, setIsSidebar] = useState(true);
  const location = useLocation();
  const [loggedIn,setLoggedIn] = useState(false);
  const [clear, setClear] = useState(false);
  
  const showTopbarAndSidebar = location.pathname !== '/';

  const settingLogin = (value) => {
    setLoggedIn(value);
  }

  const clearStorage = (value) => {
    setClear(value);
  }

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <div className="app">
          {showTopbarAndSidebar && <Sidebar isSidebar={isSidebar} />}
          <main className="content">
            <Routes>
              <Route path="/" element={<LoginPage settingLogin = {settingLogin} />} />

              <Route path="/dashboard" element={loggedIn ? <Dashboard /> : <Navigate to = "/" />} />
              <Route path="/booking" element={loggedIn ? <Booking /> : <Navigate to = "/" />} />
              <Route path="/completed" element={loggedIn ? <Completed />: <Navigate to = "/" />} />
              <Route path="/news" element={loggedIn ? <News />: <Navigate to = "/" />} />
              <Route path="/approved" element={loggedIn ? <Approved /> : <Navigate to = "/" />} />
              <Route path="/cancelled" element={loggedIn ? <Cancelled /> : <Navigate to = "/" />} />
              <Route path="/records" element={loggedIn ? <Records /> : <Navigate to = "/" />} />
              <Route path="/profile" element={loggedIn ? <Profile /> : <Navigate to = "/" />} />
            </Routes>
          </main>
        </div>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
