
import {
    Box,
    useTheme
  } from "@mui/material";
  import { useEffect, useState } from "react";
  import { tokens } from "../theme";
  import GlobalIcon from "@mui/icons-material/Public";
  import LikeIcon from "@mui/icons-material/ThumbUp";
  import EventIcon from "@mui/icons-material/EventNote";
  import ApprovedIcon from "@mui/icons-material/EventAvailable";
  import RejectIcon from "@mui/icons-material/EventBusy";
  import StatBox from "./StatBox";

const Statas = () => {

const theme = useTheme();
const colors = tokens(theme.palette.mode);
const backgroundColor = colors.primary[400]; // Use the primary color from the theme

const [newsLetterCount,setNewsLetter] = useState(0);
const [cancelledBooking,setCancelledBooking] = useState(0);
const [allBooking,setAllBooking] = useState(0);
const [completed,setCompleted] = useState(0);
const [events,setEvents] = useState(0);

  const fetchAllEvents = async () => {
    await fetch("http://localhost:4001/api/v1/events/getEvents")
    .then(async (response) => {
      const responseData = await response.json();
      console.log("events ",responseData);
      if (responseData.status === 'success') {
        setEvents(responseData.data);
      }
    })
    .catch((error) => {
      console.log(error);
    })
  }

  const fetchBookedData = async () => {
    await fetch("http://localhost:4001/api/v1/news/getAllEmail")
    .then( async (response) => {
      const responseData = await response.json();
      console.log("response ", responseData);

      if (responseData.status === 'success') {
        setNewsLetter(responseData.data.length);
      } else {
        setNewsLetter(0);
      }
    })
    .catch((error) => {
      console.log("error",error);
    })
  }

  const fetchCancelledData = async () => {
    await fetch("http://localhost:4001/api/v1/booking/allBooked")
    .then( async (response) => {
      const responseData = await response.json();
      console.log("response ", responseData);

      if (responseData.status === 'success') {
        setAllBooking(responseData.data.length);
        const filteredData = responseData.data.filter(item => item.cancelled === true);
        setCancelledBooking(filteredData.length);
        const completedfiltereData = responseData.data.filter(item => item.approvalCompleted === true);
        setCompleted(completedfiltereData.length);
      }
    })
    .catch((error) => {
      console.log("error",error);
    })
  }

  useEffect(() => {
    fetchBookedData();
    fetchCancelledData();
    fetchAllEvents();
  },[])


  return (
    <>
        <Box gridColumn="span 3" backgroundColor={colors.primary[400]} display="flex" alignItems="center" justifyContent="center" borderRadius="10px">
          <StatBox
            title={events}
            subtitle="News posted"
            icon={<GlobalIcon sx={{ color: colors.greenAccent[600], fontSize: "26px" }} />}
          />
        </Box>
        <Box gridColumn="span 3" backgroundColor={colors.primary[400]} display="flex" alignItems="center" justifyContent="center" borderRadius="10px">
          <StatBox
            title={newsLetterCount}
            subtitle="News Subscription"
            icon={<LikeIcon sx={{ color: colors.greenAccent[600], fontSize: "26px" }} />}
          />
        </Box>
        <Box gridColumn="span 3" backgroundColor={colors.primary[400]} display="flex" alignItems="center" justifyContent="center" borderRadius="10px">
          <StatBox
            title={completed}
            subtitle="Events Hosted"
            icon={<EventIcon sx={{ color: colors.greenAccent[600], fontSize: "26px" }} />}
          />
        </Box>
        <Box gridColumn="span 3" backgroundColor={colors.primary[400]} display="flex" alignItems="center" justifyContent="center" borderRadius="10px">
          <StatBox
            title={allBooking}
            subtitle="Booked Package"
            icon={<ApprovedIcon sx={{ color: colors.greenAccent[600], fontSize: "26px" }} />}
          />
        </Box>
        <Box gridColumn="span 3" backgroundColor={colors.primary[400]} display="flex" alignItems="center" justifyContent="center" borderRadius="10px">
          <StatBox
            title={cancelledBooking}
            subtitle="Cancelled Package"
            icon={<RejectIcon sx={{ color: colors.greenAccent[600], fontSize: "26px" }} />}
          />
        </Box>
    </>
  )
}

export default Statas