import { Box, Typography, useTheme } from "@mui/material";
import { tokens } from "../theme";

const StatBox = ({ title, subtitle, icon }) => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  return (
    <Box width="100%" m="0 30px" >
      <Box display="flex" justifyContent="space-between">
        <Box display="flex" justifyContent="center" mt="2px" alignItems={'center'}>
          <Typography variant="h4" sx={{ color: colors.grey[100] }} paddingRight={'10px'}>
            {subtitle}
          </Typography>
        </Box>
        <Box>
          <Typography
              variant="h4"
              fontWeight="bold"
              paddingBottom={'40px'}
              sx={{ color: colors.greenAccent[500] }}
            >
              {title}
          </Typography>
          <Typography align="center">
            {icon}
          </Typography>
        </Box>
      </Box>

    </Box>
  );
};

export default StatBox;
