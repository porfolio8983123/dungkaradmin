import {
  Box,
  Typography,
  useTheme,
  List,
  ListItem,
  ListItemText,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
  IconButton,
  MenuItem,
  Select,
  FormControl,
  InputLabel
} from "@mui/material";
import { useEffect, useState } from "react";
import { tokens } from "../../theme";
import DeleteIcon from "@mui/icons-material/Delete";
import Header from "../../components/Header";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import listPlugin from "@fullcalendar/list";
import Statas from "../../components/Statas";
import { formatDate } from "@fullcalendar/core/index.js";
import Topbar from "../global/Topbar";
import { ToastContainer, toast, Bounce } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Dashboard = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const backgroundColor = colors.primary[400]; // Use the primary color from the theme
  const [currentEvents, setCurrentEvents] = useState([]);
  const [open, setOpen] = useState(false);
  const [newEvent, setNewEvent] = useState({ title: "", date: "", time: "", organization: "", room: "" });
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [eventToDelete, setEventToDelete] = useState(null);
  const [sortedEvents,setSortedEvents] = useState([]);
  const [today,setToday] = useState('');

  const fetchEvents = async () => {
    await fetch("http://localhost:4001/api/v1/events/get",{
      method:"POST",
      headers: {
        'Content-Type':'application/json'
      },
      body: JSON.stringify({date: today})
    })
    .then( async (response) => {
      const responseData = await response.json();
      console.log("response ", responseData);

      if (responseData.status === 'success') {
        setSortedEvents(responseData.data);
      }
    })
    .catch((error) => {
      console.log("error",error);
    })
  }

  const getTodaysDate = () => {
    const today = new Date();
    const year = today.getFullYear();
    const month = String(today.getMonth() + 1).padStart(2, '0');
    const day = String(today.getDate()).padStart(2, '0');
    setToday(`${year}-${month}-${day}`);

  };

  useEffect(() => {
    getTodaysDate();
  },[])

  useEffect(() => {
    fetchEvents();
  },[today])

  useEffect(() => {
    console.log("today ",today);
  },[today])

  const handleDateClick = (selected) => {
    // setNewEvent({ ...newEvent, date: selected.startStr });
    console.log("clicked",selected);
    setToday(selected.startStr);
  };

  const handleEventClick = (selected) => {
    // Do nothing
  };

  const handleFormSubmit = async () => {
    // const calendarApi = window.calendarApi;
    // if (newEvent.title) {
    //   calendarApi.addEvent({
    //     id: `${newEvent.date}-${newEvent.title}`,
    //     title: newEvent.title,
    //     start: `${newEvent.date}T${newEvent.time}`,
    //     allDay: false,
    //     extendedProps: { organization: newEvent.organization, room: newEvent.room },
    //     backgroundColor: backgroundColor,
    //     borderColor: backgroundColor
    //   });
    //   setNewEvent({ title: "", date: "", time: "", organization: "", room: "" });
    // }
    // setOpen(false);
    console.log("hello");

    await fetch("http://localhost:4001/api/v1/events/add",{
      method:"POST",
      headers: {
        "Content-Type":"application/json"
      },
      body: JSON.stringify(newEvent)
    })
    .then(async (response) => {
      const responseData = await response.json();
      console.log(responseData);
      if (responseData.status === 'success') {
        toast.success(responseData.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          transition: Bounce,
        });
      }

      fetchEvents();
    })
    .catch((error) => {
      console.log(error);
      toast.success(error.error, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        transition: Bounce,
      });
    })
  };

  const handleDeleteEvent = async (id) => {
    setEventToDelete(id);
    setDeleteDialogOpen(true);
  };

  const confirmDeleteEvent = async () => {
    await fetch(`http://localhost:4001/api/v1/events/events/${eventToDelete}`,{
      method:"DELETE"
    })
    .then(async (response) => {
      const responseData = await response.json();
      console.log("deleting ",responseData);

      if (responseData.status === 'success') {
        fetchEvents();
      }

    })
    .catch((error) => {
      console.log(error);
    })
    setDeleteDialogOpen(false);
    setEventToDelete(null);
  };

  const openAddEventDialog = () => {
    setNewEvent({ title: "", date: "", time: "", organization: "", room: "" });
    setOpen(true);
  };

  const isFormValid = () => {
    return (
      newEvent.title &&
      newEvent.date &&
      newEvent.time &&
      newEvent.organization &&
      newEvent.room
    );
  };

  return (
    <>
      <Topbar />
      <Box m="20px">
      {/* HEADER */}
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Header title="DASHBOARD" subtitle="Welcome to your dashboard" />
      </Box>

      {/* GRID & CHARTS */}
      <Box display="grid" gridTemplateColumns="repeat(15, 1fr)" gridAutoRows="140px" gap="20px">
        {/* ROW 1 */}
        
        <Statas />

        {/* ROW 2 */}
        <Box display="flex" justifyContent="space-between" gridColumn="span 15">
          {/* CALENDAR SIDEBAR */}
          <Box flex="1 1 19%" backgroundColor={colors.primary[400]} p="10px" borderRadius="4px" mr="20px" height={'50vh'} overflow="hidden">
            <Typography variant="h5">Events</Typography>
            <Box display="flex" justifyContent="center" mb="10px" paddingTop='10px'>
              <Button variant="contained" onClick={openAddEventDialog} sx={{ backgroundColor: '#FE3600', color: '#fff' }}>
                Add Event
              </Button>
            </Box>
            <Box sx={{ height: 'calc(100% - 80px)', overflowY: 'auto', padding: '0 10px' }}>
              <List sx={{ width: '100%' }}>
                {sortedEvents.map((event) => (
                  <ListItem key={event._id} sx={{ backgroundColor: '#FE3600', margin: "10px 0", borderRadius: "2px", flexDirection: 'column' }}>
                    <ListItemText
                      primary={event.title}
                      secondary={
                        <>
                          <Typography component="span" sx={{ display: 'block', whiteSpace: 'normal' }}>
                            {formatDate(event.date, {
                              year: "numeric",
                              month: "short",
                              day: "numeric",
                              hour: "2-digit",
                              minute: "2-digit",
                            })}
                          </Typography>
                          <Typography component="span" sx={{ display: 'block', whiteSpace: 'normal' }}>
                            {event.organization}
                          </Typography>
                          <Typography component="span" sx={{ display: 'block', whiteSpace: 'normal' }}>
                            Room: {event.room}
                          </Typography>
                        </>
                      }
                      sx={{ color: 'white' }}
                    />
                    <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteEvent(event._id)} sx={{ color: 'white' }}>
                      <DeleteIcon />
                    </IconButton>
                  </ListItem>
                ))}
              </List>
            </Box>
          </Box>

          {/* CALENDAR */}
          <Box flex="1 1 80%" ml="25px">
            <FullCalendar
              height="50vh"
              plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin, listPlugin]}
              headerToolbar={{
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
              }}
              initialView="dayGridMonth"
              editable={true}
              selectable={true}
              selectMirror={true}
              dayMaxEvents={true} // This will limit the number of events displayed per day
              dayMaxEventRows={true} // This will show the "more" link if there are too many events
              select={handleDateClick}
              eventClick={handleEventClick}
              eventsSet={(events) => setCurrentEvents(events)}
              initialEvents={[]}
              ref={(calendar) => {
                window.calendarApi = calendar?.getApi();
              }}
              eventContent={(eventInfo) => (
                <div
                  style={{
                    backgroundColor: backgroundColor,
                    borderColor: backgroundColor,
                    color: theme.palette.getContrastText(backgroundColor),
                    padding: '2px 4px',
                    borderRadius: '2px',
                    textAlign: 'center',
                    verticalAlign: 'middle',
                    fontSize: '1.5',
                  }}
                >
                  <strong>{eventInfo.event.title}</strong>
                  <br />
                  <span>{eventInfo.timeText}</span>
                </div>
              )}
            />
          </Box>
        </Box>
      </Box>

      {/* EVENT DIALOG */}
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>Add New Event</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Event Title"
            fullWidth
            value={newEvent.title}
            onChange={(e) => setNewEvent({ ...newEvent, title: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Event Date"
            type="date"
            fullWidth
            value={newEvent.date}
            onChange={(e) => setNewEvent({ ...newEvent, date: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              shrink: true,
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Event Time"
            type="time"
            fullWidth
            value={newEvent.time}
            onChange={(e) => setNewEvent({ ...newEvent, time: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              shrink: true,
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Organization"
            fullWidth
            value={newEvent.organization}
            onChange={(e) => setNewEvent({ ...newEvent, organization: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <FormControl fullWidth margin="dense">
            <InputLabel style={{ color: '#FE3600' }}>Choose a Room</InputLabel>
            <Select
              value={newEvent.room}
              onChange={(e) => setNewEvent({ ...newEvent, room: e.target.value })}
              label="Choose a Room"
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
              inputProps={{
                style: {
                  color: 'black',
                }
              }}
            >
              <MenuItem value="Seminar 1">Seminar 1</MenuItem>
              <MenuItem value="Seminar 2">Seminar 2</MenuItem>
              <MenuItem value="Seminar 3">Seminar 3</MenuItem>
            </Select>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} style={{ color: '#FE3600' }}>
            Cancel
          </Button>
          <Button onClick={handleFormSubmit} style={{ color: '#FE3600' }} disabled={!isFormValid()}>
            Add Event
          </Button>
        </DialogActions>
      </Dialog>

      {/* DELETE CONFIRMATION DIALOG */}
      <Dialog open={deleteDialogOpen} onClose={() => setDeleteDialogOpen(false)}>
        <DialogTitle>Confirm Deletion</DialogTitle>
        <DialogContent>
          <Typography>Are you sure you want to delete this event?</Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDeleteDialogOpen(false)} style={{ color: '#FE3600' }}>
            Cancel
          </Button>
          <Button onClick={confirmDeleteEvent} style={{ color: '#FE3600' }}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
    <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        transition={Bounce}
      />
    </>
  );
};

export default Dashboard;
