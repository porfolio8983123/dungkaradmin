import {
  Box,
  Typography,
  useTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  MenuItem,
  Select,
  FormControl,
  InputLabel
} from "@mui/material";
import { useEffect, useState } from "react";
import { tokens } from "../../theme";
import GlobalIcon from "@mui/icons-material/Public";
import LikeIcon from "@mui/icons-material/ThumbUp";
import EventIcon from "@mui/icons-material/EventNote";
import ApprovedIcon from "@mui/icons-material/EventAvailable";
import RejectIcon from "@mui/icons-material/EventBusy";
import StatBox from "../../components/StatBox";
import Statas from "../../components/Statas";
import Topbar from "../global/Topbar";

const Records = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [currentBookings, setCurrentBookings] = useState([]);
  const [open, setOpen] = useState(false);
  const [newBooking, setNewBooking] = useState({ name: "", cid: "", mobile: "", email: "", organization: "", category: "", package: "", date: "", time: "", remarks: "", hall: "Seminar Hall 1" });
  const [selectedHall, setSelectedHall] = useState("Meditation Hall 1");
  const [filteredBookings,setFilteredBookings] = useState([]);

  const fetchBookedData = async () => {
    await fetch("http://localhost:4001/api/v1/booking/allBooked")
    .then( async (response) => {
      const responseData = await response.json();
      console.log("response ", responseData);

      if (responseData.status === 'success') {
        setCurrentBookings(responseData.data);
      }
    })
    .catch((error) => {
      console.log("error",error);
    })
  }

  useEffect(() => {
    fetchBookedData();
  },[])

  useEffect(() => {
    const filteredData = currentBookings.filter(item => item.conferenceId.name === selectedHall);
    console.log("filtered data ",filteredData);
    setFilteredBookings(filteredData)
  },[currentBookings,selectedHall])

  const handleFormSubmit = () => {
    setCurrentBookings([...currentBookings, { ...newBooking }]);
    setNewBooking({ name: "", cid: "", mobile: "", email: "", organization: "", category: "", package: "", date: "", time: "", remarks: "", hall: "Seminar Hall 1" });
    setOpen(false);
  };

  const openAddBookingDialog = () => {
    setNewBooking({ name: "", cid: "", mobile: "", email: "", organization: "", category: "", package: "", date: "", time: "", remarks: "", hall: "Seminar Hall 1" });
    setOpen(true);
  };

  const formatDate = (dateStr) => {
    const date = new Date(dateStr);
    return date.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' });
  }

  const formatTime = (timeStr) => {
    if (!timeStr) return ""; // Handle case where timeStr is undefined or null

    const timeParts = timeStr.split(':');
    if (timeParts.length !== 2) return ""; // Handle improperly formatted timeStr

    let [hour, minute] = timeParts.map(Number);
    if (isNaN(hour) || isNaN(minute)) return ""; // Handle non-numeric values

    const ampm = hour >= 12 ? 'PM' : 'AM';
    hour = hour % 12 || 12; // Convert hour to 12-hour format

    return `${hour}:${minute.toString().padStart(2, '0')} ${ampm}`;
  };

  return (
    <>
      <Topbar />
      <Box m="20px">
      <Box display="grid" gridTemplateColumns="repeat(15, 1fr)" gridAutoRows="140px" gap="20px">
        {/* ROW 1 */}
        <Statas />
      </Box>

      {/* ROW 2 */}
      <Box display="flex" justifyContent="space-between" gridColumn="span 15" mb="10px" mt="20px">
        {/* SEMINAR HALL SELECTION */}
        <Box display="flex" alignItems="center">
          <FormControl variant="outlined" sx={{ minWidth: 120, backgroundColor: colors.primary[400] }}>
            <InputLabel sx={{ color: colors.greenAccent[600] }}>Seminar Hall</InputLabel>
            <Select
              value={selectedHall}
              onChange={(e) => setSelectedHall(e.target.value)}
              label="Seminar Hall"
              sx={{ color: colors.greenAccent[600], borderColor: colors.primary[400] }}
            >
              <MenuItem value="Meditation Hall 1">Meditation Hall 1</MenuItem>
              <MenuItem value="Conference1">Conference1</MenuItem>
              <MenuItem value="Conference4">Conference4</MenuItem>
            </Select>
          </FormControl>
        </Box>
      </Box>

      {/* BOOKINGS TABLE */}
      <Box flex="1 1 80%" mt="10px">
        <Typography variant="h5" gutterBottom>{selectedHall}</Typography>
        <TableContainer component={Paper} sx={{ height: '50vh', overflowY: 'auto', backgroundColor: colors.primary[400] }}>
          <Table stickyHeader>
            <TableHead>
              <TableRow>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Name</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>CID</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Mobile No</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Email</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Organization</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Category</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Package</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Date</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Starting Time</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Ending Time</TableCell>
                <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Remarks</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredBookings.map((booking, index) => (
                <TableRow key={index}>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.name}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.CID}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.phone}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.email}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.organization}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.conferenceId.name}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.package === true ? "true" : "false"}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{formatDate(booking.date)}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{formatTime(booking.startingTime)}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{formatTime(booking.endingTime)}</TableCell>
                  <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.cancelled === true ? "Cancelled" : "Approved"}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Box>

      {/* BOOKING DIALOG */}
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>Add New Booking</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Name"
            fullWidth
            value={newBooking.name}
            onChange={(e) => setNewBooking({ ...newBooking, name: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="CID"
            fullWidth
            value={newBooking.cid}
            onChange={(e) => setNewBooking({ ...newBooking, cid: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Mobile"
            fullWidth
            value={newBooking.mobile}
            onChange={(e) => setNewBooking({ ...newBooking, mobile: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Email"
            fullWidth
            value={newBooking.email}
            onChange={(e) => setNewBooking({ ...newBooking, email: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Organization"
            fullWidth
            value={newBooking.organization}
            onChange={(e) => setNewBooking({ ...newBooking, organization: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Category"
            fullWidth
            value={newBooking.category}
            onChange={(e) => setNewBooking({ ...newBooking, category: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Package"
            fullWidth
            value={newBooking.package}
            onChange={(e) => setNewBooking({ ...newBooking, package: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Date"
            fullWidth
            value={newBooking.date}
            onChange={(e) => setNewBooking({ ...newBooking, date: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Time"
            fullWidth
            value={newBooking.time}
            onChange={(e) => setNewBooking({ ...newBooking, time: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Remarks"
            fullWidth
            value={newBooking.remarks}
            onChange={(e) => setNewBooking({ ...newBooking, remarks: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="primary">Cancel</Button>
          <Button onClick={handleFormSubmit} color="primary">Add</Button>
        </DialogActions>
      </Dialog>
    </Box>
    </>
  );
};

export default Records;
