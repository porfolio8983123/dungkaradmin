import React, { useEffect, useState } from 'react';
import EyeOpen from '../../Img/eye-open.svg'; // Import the open eye SVG icon
import EyeClosed from '../../Img/eye-closed.svg'; // Import the closed eye SVG icon
import { ToastContainer, toast, Bounce } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom';

const LoginPage = ({settingLogin}) => {
  const [showPassword, setShowPassword] = useState(false); // State to manage password visibility
  const [eyeIcon, setEyeIcon] = useState(EyeClosed); // State to manage eye icon
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const styles = {
    container: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
      backgroundColor: '#ffffff',
    },
    box: {
      textAlign: 'center',
      padding: '20px',
    },
    logo: {
      width: '100px', // Adjust size as needed
    },
    title: {
      color: '#ff4500',
      fontSize: '24px',
      margin: '20px 0',
    },
    inputGroup: {
      marginBottom: '20px',
      position: 'relative',
    },
    inputField: {
      width: '100%',
      padding: '10px',
      boxSizing: 'border-box',
    },
    togglePassword: {
      position: 'absolute',
      right: '10px',
      top: '50%',
      transform: 'translateY(-50%)',
      background: 'none',
      border: 'none',
      cursor: 'pointer',
    },
    eyeIcon: {
      width: '20px', // Adjust the size of the eye icon
      height: 'auto',
    },
    loginButton: {
      backgroundColor: '#ff4500',
      color: 'white',
      padding: '10px 20px',
      border: 'none',
      cursor: 'pointer',
    },
  };

  const togglePasswordVisibility = () => {
    setShowPassword(!showPassword); // Toggle password visibility state
    setEyeIcon(showPassword ? EyeClosed : EyeOpen); // Toggle eye icon
  };

  useEffect(() => {
    console.log(username);
    console.log(password);
  }, [username, password]);

  const isFormValid = () => {
    return username.trim() !== "" && password.trim() !== "";
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (isFormValid()) {
      // Handle login logic here
      await fetch("http://localhost:4001/api/v1/admin/login", {
        method: "POST",
        headers: {
          "Content-Type":"application/json"
        },
        body: JSON.stringify({username,password})
      })
      .then(async (response) => {
        const responseData = await response.json();
        console.log(responseData);

        if (responseData.status === 'error') {
          toast.error(responseData.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
            transition: Bounce,
          });
        } else {
          settingLogin(true);
          localStorage.setItem("token",responseData.token);
          localStorage.setItem("username",responseData.data.username)
          navigate('/dashboard');
        }

      })
      .catch((error) => {
        console.log(error);
      })
    }
  };

  return (
    <div style={styles.container}>
      <div style={styles.box}>
        <img src={`../../assets/Logo.svg`} alt="Logo" style={styles.logo} /> {/* Replace with your actual logo path */}
        <h1 style={styles.title}>Login as Admin</h1>
        <form onSubmit={handleSubmit}>
          <div style={styles.inputGroup}>
            <input type="text" placeholder="Username" style={styles.inputField}
                value={username}
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
            />
          </div>
          <div style={styles.inputGroup}>
            <input
              type={showPassword ? 'text' : 'password'}
              placeholder="Password"
              style={styles.inputField}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <button type="button" style={styles.togglePassword} onClick={togglePasswordVisibility}>
              <img src={eyeIcon} alt="Toggle Password Visibility" style={styles.eyeIcon} />
            </button>
          </div>
          <button type="submit" style={styles.loginButton} disabled={!isFormValid()}>Login</button>
        </form>
      </div>

      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        transition={Bounce}
      />
    </div>
  );
};

export default LoginPage;
