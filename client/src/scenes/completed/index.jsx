import {
    Box,
    Typography,
    useTheme,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper
  } from "@mui/material";
  import { useEffect, useState } from "react";
  import { tokens } from "../../theme";
  import Statas from "../../components/Statas";
import Topbar from "../global/Topbar";
  
  const Completed = () => {
    const theme = useTheme();
    const colors = tokens(theme.palette.mode);
    const [currentBookings, setCurrentBookings] = useState([]);
    const [completedBookings, setCompletedBookings] = useState([]);
    const [open, setOpen] = useState(false);
    const [newBooking, setNewBooking] = useState({ name: "", cid: "", mobile: "", email: "", organization: "", category: "", package: "", date: "", time: "", remarks: "" });

    const fetchBookedData = async () => {
      await fetch("http://localhost:4001/api/v1/booking/allBooked")
      .then( async (response) => {
        const responseData = await response.json();
        console.log("response ", responseData);

        if (responseData.status === 'success') {
          const filteredData = responseData.data.filter(item => item.status === true && item.approvalCompleted === true && item.cancelled === false);
          setCompletedBookings(filteredData);
        }
      })
      .catch((error) => {
        console.log("error",error);
      })
    }

    useEffect(() => {
      fetchBookedData();
    },[])

    const formatDate = (dateStr) => {
      const date = new Date(dateStr);
      return date.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' });
    }

    const formatTime = (timeStr) => {
      if (!timeStr) return ""; // Handle case where timeStr is undefined or null
  
      const timeParts = timeStr.split(':');
      if (timeParts.length !== 2) return ""; // Handle improperly formatted timeStr
  
      let [hour, minute] = timeParts.map(Number);
      if (isNaN(hour) || isNaN(minute)) return ""; // Handle non-numeric values
  
      const ampm = hour >= 12 ? 'PM' : 'AM';
      hour = hour % 12 || 12; // Convert hour to 12-hour format
  
      return `${hour}:${minute.toString().padStart(2, '0')} ${ampm}`;
    };
  
    const handleFormSubmit = () => {
      setCurrentBookings([...currentBookings, { ...newBooking }]);
      setNewBooking({ name: "", cid: "", mobile: "", email: "", organization: "", category: "", package: "", date: "", time: "", remarks: "" });
      setOpen(false);
    };
  
    const handleApprove = (index) => {
      const bookings = [...currentBookings];
      bookings[index].status = "approved";
      setCurrentBookings(bookings);
    };
  
    const handleDecline = (index) => {
      const bookings = [...currentBookings];
      bookings[index].status = "declined";
      setCurrentBookings(bookings);
    };
  
    const openAddBookingDialog = () => {
      setNewBooking({ name: "", cid: "", mobile: "", email: "", organization: "", category: "", package: "", date: "", time: "", remarks: "" });
      setOpen(true);
    };
  
    return (
     <>
      <Topbar />
       <Box m="20px">
        <Box display="grid" gridTemplateColumns="repeat(15, 1fr)" gridAutoRows="140px" gap="20px">
          {/* ROW 1 */}
         <Statas />
  
          {/* ROW 2 */}
          <Box display="flex" justifyContent="space-between" gridColumn="span 15">
            {/* COMPLETED BOOKINGS TABLE */}
            <Box flex="1 1 80%" ml="25px" mt="25px">
              <Typography variant="h5" gutterBottom>Completed</Typography>
              <TableContainer component={Paper} sx={{ height: '50vh', overflowY: 'auto', backgroundColor: colors.primary[400] }}>
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Name</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>CID</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Mobile No</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Email</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Organization</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Category</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Package</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Date</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Starting Time</TableCell>
                      <TableCell sx={{ backgroundColor: colors.primary[400], color: colors.greenAccent[600] }}>Ending Time</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {completedBookings.map((booking, index) => (
                      <TableRow key={index}>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.name}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.CID}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.phone}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.email}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.userId.organization}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.conferenceId.name}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{booking.package === true ? "true" : "false"}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{formatDate(booking.date)}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{formatTime(booking.startingTime)}</TableCell>
                        <TableCell sx={{ color: colors.greenAccent[600] }}>{formatTime(booking.endingTime)}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
          </Box>
        </Box>
  
        {/* BOOKING DIALOG */}
        <Dialog open={open} onClose={() => setOpen(false)}>
          <DialogTitle>Add New Booking</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin="dense"
              label="Name"
              fullWidth
              value={newBooking.name}
              onChange={(e) => setNewBooking({ ...newBooking, name: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="CID"
              fullWidth
              value={newBooking.cid}
              onChange={(e) => setNewBooking({ ...newBooking, cid: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Mobile No"
              fullWidth
              value={newBooking.mobile}
              onChange={(e) => setNewBooking({ ...newBooking, mobile: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Email"
              fullWidth
              value={newBooking.email}
              onChange={(e) => setNewBooking({ ...newBooking, email: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Organization"
              fullWidth
              value={newBooking.organization}
              onChange={(e) => setNewBooking({ ...newBooking, organization: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Category"
              fullWidth
              value={newBooking.category}
              onChange={(e) => setNewBooking({ ...newBooking, category: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Package"
              fullWidth
              value={newBooking.package}
              onChange={(e) => setNewBooking({ ...newBooking, package: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Date"
              type="date"
              fullWidth
              value={newBooking.date}
              onChange={(e) => setNewBooking({ ...newBooking, date: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                shrink: true,
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Time"
              type="time"
              fullWidth
              value={newBooking.time}
              onChange={(e) => setNewBooking({ ...newBooking, time: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                shrink: true,
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
            <TextField
              margin="dense"
              label="Remarks"
              fullWidth
              value={newBooking.remarks}
              onChange={(e) => setNewBooking({ ...newBooking, remarks: e.target.value })}
              InputProps={{
                style: {
                  color: 'black',
                }
              }}
              InputLabelProps={{
                style: {
                  color: '#FE3600',
                }
              }}
              sx={{
                '& .MuiOutlinedInput-root': {
                  '& fieldset': {
                    borderColor: 'black',
                  },
                  '&:hover fieldset': {
                    borderColor: '#FE3600',
                  },
                  '&.Mui-focused fieldset': {
                    borderColor: '#FE3600',
                  },
                },
              }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setOpen(false)} style={{ color: '#FE3600' }}>
              Cancel
            </Button>
            <Button onClick={handleFormSubmit} style={{ color: '#FE3600' }}>
              Add Booking
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
     </>
    );
  };
  
  export default Completed;
  