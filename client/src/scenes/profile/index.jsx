import {
  Box,
  Typography,
  useTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
} from "@mui/material";
import { useEffect, useState } from "react";
import { tokens } from "../../theme";
import Statas from "../../components/Statas";
import Logo from '../../Img/Logo.svg';
import Topbar from "../global/Topbar";
import { ToastContainer, toast, Bounce } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Profile = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [open, setOpen] = useState(false);
  const [openDetails, setOpenDetails] = useState(false);
  const [profileData, setProfileData] = useState({ name: "John Doe", profilePic: Logo, password: "", confirmPassword: "",oldPassword:"" });
  const [userName, setUserName] = useState(profileData.name);

  const handlePasswordSubmit = async () => {
    // Handle password update logic here
    await fetch("http://localhost:4001/api/v1/admin/changePassword", {
      method: "POST",
      headers: {
        "Content-Type":"application/json"
      },
      body: JSON.stringify({
        username:profileData.name,
        oldPassword: profileData.oldPassword,
        password: profileData.password,
        confirmPassword: profileData.confirmPassword
      })
    })
    .then(async (response) => {
      const responseData = await response.json();
      console.log("response ",responseData);
      if (responseData.status === 'success') {
        console.log("hello")
        toast.success(responseData.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          transition: Bounce,
        });
        setProfileData({...profileData,oldPassword:"",confirmPassword:"",password:""})
        setOpen(false);
      } else {
        toast.error(responseData.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
          transition: Bounce,
        });
      }
    })
    .catch((error) => {
      console.log("error ",error);
      toast.error(error.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        transition: Bounce,
      });
    })
  };

  const handleDetailsSubmit = () => {
    setProfileData({ ...profileData, name: userName });
    setOpenDetails(false);
  };

  useEffect(() => {
    let username = localStorage.getItem("username");

    setProfileData({ ...profileData, name: username });

  },[])

  const openEditPasswordDialog = () => {
    setOpen(true);
  };

  const openEditDetailsDialog = () => {
    setOpenDetails(true);
  };

  useEffect(() => {
    console.log("profile data ",profileData);
  },[profileData]);

  return (
    <>
      <Topbar />
      <Box m="20px">
      <Box display="grid" gridTemplateColumns="repeat(15, 1fr)" gridAutoRows="140px" gap="20px">
        {/* ROW 1 */}
        <Statas />

        {/* ROW 2 */}
        <Box display="flex" justifyContent="space-between" gridColumn="span 15">
          {/* PROFILE SECTION */}
          <Box flex="1 1 80%" ml="25px" mt="25px">
            <Typography variant="h5" gutterBottom>Profile</Typography>
            <Box display="flex" flexDirection="column" alignItems="center" mt="20px">
              <Box component="img" src={profileData.profilePic} alt="Logo" sx={{ width: 100, height: 100, mb: 2 }} />
              <Typography variant="h6" gutterBottom>{profileData.name}</Typography>
              <Box display="flex" gap="10px">
                <Button variant="contained" color="primary" onClick={openEditPasswordDialog}>Change Password</Button>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>

      {/* CHANGE PASSWORD DIALOG */}
      <Dialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>Change Password</DialogTitle>
        <DialogContent>
        <TextField
            margin="dense"
            label="Old Password"
            type="password"
            fullWidth
            value={profileData.oldPassword}
            onChange={(e) => setProfileData({ ...profileData, oldPassword: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="New Password"
            type="password"
            fullWidth
            value={profileData.password}
            onChange={(e) => setProfileData({ ...profileData, password: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
          <TextField
            margin="dense"
            label="Confirm Password"
            type="password"
            fullWidth
            value={profileData.confirmPassword}
            onChange={(e) => setProfileData({ ...profileData, confirmPassword: e.target.value })}
            InputProps={{
              style: {
                color: 'black',
              }
            }}
            InputLabelProps={{
              style: {
                color: '#FE3600',
              }
            }}
            sx={{
              '& .MuiOutlinedInput-root': {
                '& fieldset': {
                  borderColor: 'black',
                },
                '&:hover fieldset': {
                  borderColor: '#FE3600',
                },
                '&.Mui-focused fieldset': {
                  borderColor: '#FE3600',
                },
              },
            }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} style={{ color: '#FE3600' }}>
            Cancel
          </Button>
          <Button onClick={handlePasswordSubmit} style={{ color: '#FE3600' }}>
            Save Changes
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
    <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        transition={Bounce}
      />
    </>
  );
};

export default Profile;
