import {
  Box,
  Typography,
  useTheme,
  List,
  ListItem,
  ListItemText,
  TextField,
  Button,
  IconButton,
  InputLabel,
  Input,
  ListItemSecondaryAction,
  FormControl,
  Select,
  MenuItem
} from "@mui/material";
import { useEffect, useState } from "react";
import { tokens } from "../../theme";
import DeleteIcon from "@mui/icons-material/Delete";
import Statas from "../../components/Statas";
import Topbar from "../global/Topbar";
import { ToastContainer, toast, Bounce } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const News = () => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const backgroundColor = colors.primary[400];
  const [newsList, setNewsList] = useState([]);
  const [newNews, setNewNews] = useState({
    title: "",
    description: "",
    date: "",
    coverPhoto: "",
    morePhotos: [],
    room: ""
  });
  const [view, setView] = useState('form'); // 'form' or 'list'
  const [sortedNews, setSortedNews] = useState([]);

  useEffect(() => {
    console.log(newNews);
  }, [newNews]);

  const handleDeleteNews = async (id) => {
    await fetch(`http://localhost:4001/api/v1/currentNews/deleteNews/${id}`, {
      method: "DELETE",
    })
      .then(async (response) => {
        const responseData = await response.json();
        console.log("response ", responseData);

        if (responseData.status === 'success') {
          toast.success(responseData.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
            transition: Bounce,
          });
          setSortedNews((prev) => prev.filter((news) => news._id !== id));
        }
      })
      .catch((error) => {
        console.log("error ", error);
      });
  };

  const handleCoverPhotoChange = async (e) => {
    const file = e.target.files[0];
    const base64 = await convertToBase64(file);

    console.log(base64);

    setNewNews({ ...newNews, coverPhoto: base64 });
  };

  const handleMorePhotosChange = async (e) => {
    const files = Array.from(e.target.files).slice(0, 8);
    const base64Files = await Promise.all(files.map(file => convertToBase64(file)));
    console.log("Base64 images", base64Files);
    setNewNews({ ...newNews, morePhotos: base64Files });
  };

  const isFormValid = () => {
    return (
      newNews.title &&
      newNews.description &&
      newNews.date &&
      newNews.coverPhoto &&
      newNews.morePhotos.length > 0
    );
  };

  const convertToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  const handleGetNews = async () => {
    await fetch("http://localhost:4001/api/v1/currentNews/getAllNews")
      .then(async (response) => {
        const responseData = await response.json();
        console.log("news data ", responseData);

        if (responseData.status === 'success') {
          setSortedNews(responseData.data);
        }
      })
      .catch((error) => {
        console.log("error ", error);
      });
  };

  useEffect(() => {
    handleGetNews();
  }, []); // Only fetch news once on mount

  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log("hello man");
    await fetch("http://localhost:4001/api/v1/currentNews/add", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newNews),
    })
      .then(async (response) => {
        const responseData = await response.json();
        console.log("response ", responseData);

        if (responseData.status === 'success') {
          toast.success(responseData.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
            transition: Bounce,
          });

          handleGetNews(); // Refresh the news list
          setView('list'); // Switch to the list view
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const formatDate = (dateStr) => {
    const date = new Date(dateStr);
    return date.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' });
  };

  return (
    <>
      <Topbar />
      <Box m="20px">
        <Box display="grid" gridTemplateColumns="repeat(15, 1fr)" gridAutoRows="140px" gap="20px">
          <Statas />

          {/* ROW 2 */}
          <Box display="flex" justifyContent="space-between" gridColumn="span 15" flexDirection="column">
            {/* Toggle Buttons */}
            <Box display="flex" justifyContent="flex-end" mb={2}>
              <Button
                variant="contained"
                sx={{ backgroundColor: view === 'form' ? '#FE3600' : colors.primary[600], color: '#fff', marginRight: 1 }}
                onClick={() => setView('form')}
              >
                Add News
              </Button>
              <Button
                variant="contained"
                sx={{ backgroundColor: view === 'list' ? '#FE3600' : colors.primary[600], color: '#fff' }}
                onClick={() => setView('list')}
              >
                News List
              </Button>
            </Box>

            {/* Conditional Rendering based on view */}
            {view === 'form' ? (
              <Box backgroundColor={backgroundColor} p="10px" borderRadius="4px">
                <Typography variant="h5">Add New News</Typography>
                <Box component="form" onSubmit={handleSubmit}>
                  <TextField
                    margin="dense"
                    label="News/Event Title"
                    fullWidth
                    value={newNews.title}
                    onChange={(e) => setNewNews({ ...newNews, title: e.target.value })}
                    InputProps={{
                      style: {
                        color: 'black',
                      }
                    }}
                    InputLabelProps={{
                      style: {
                        color: '#FE3600',
                      }
                    }}
                    sx={{
                      '& .MuiOutlinedInput-root': {
                        '& fieldset': {
                          borderColor: 'black',
                        },
                        '&:hover fieldset': {
                          borderColor: '#FE3600',
                        },
                        '&.Mui-focused fieldset': {
                          borderColor: '#FE3600',
                        },
                      },
                    }}
                  />
                  <TextField
                    margin="dense"
                    label="Description"
                    multiline
                    rows={1}
                    fullWidth
                    value={newNews.description}
                    onChange={(e) => setNewNews({ ...newNews, description: e.target.value })}
                    InputProps={{
                      style: {
                        color: 'black',
                      }
                    }}
                    InputLabelProps={{
                      style: {
                        color: '#FE3600',
                      }
                    }}
                    sx={{
                      '& .MuiOutlinedInput-root': {
                        '& fieldset': {
                          borderColor: 'black',
                        },
                        '&:hover fieldset': {
                          borderColor: '#FE3600',
                        },
                        '&.Mui-focused fieldset': {
                          borderColor: '#FE3600',
                        },
                      },
                    }}
                  />
                  <TextField
                    margin="dense"
                    label="Select Date"
                    type="date"
                    fullWidth
                    value={newNews.date}
                    onChange={(e) => setNewNews({ ...newNews, date: e.target.value })}
                    InputProps={{
                      style: {
                        color: 'black',
                      }
                    }}
                    InputLabelProps={{
                      shrink: true, // This line forces the label to always shrink
                      style: {
                        color: '#FE3600',
                      }
                    }}
                    sx={{
                      '& input::-webkit-datetime-edit, & input::-webkit-inner-spin-button, & input::-webkit-clear-button': {
                        color: 'black',
                        position: 'relative',
                        left: '-5px', // Adjust as needed
                      },
                      '& input[type="date"]': {
                        '&::-webkit-calendar-picker-indicator': {
                          filter: 'invert(1)', // Inverts the color of the calendar icon for better visibility
                        },
                      },
                      '& .MuiOutlinedInput-root': {
                        '& fieldset': {
                          borderColor: 'black',
                        },
                        '&:hover fieldset': {
                          borderColor: '#FE3600',
                        },
                        '&.Mui-focused fieldset': {
                          borderColor: '#FE3600',
                        },
                      },
                    }}
                  />

                  <FormControl fullWidth margin="dense">
                    <InputLabel style={{ color: '#FE3600' }}>Choose a Room</InputLabel>
                    <Select
                      value={newNews.room}
                      onChange={(e) => setNewNews({ ...newNews, room: e.target.value })}
                      label="Choose a Room"
                      sx={{
                        '& .MuiOutlinedInput-root': {
                          '& fieldset': {
                            borderColor: 'black',
                          },
                          '&:hover fieldset': {
                            borderColor: '#FE3600',
                          },
                          '&.Mui-focused fieldset': {
                            borderColor: '#FE3600',
                          },
                        },
                      }}
                      inputProps={{
                        style: {
                          color: 'black',
                        }
                      }}
                    >
                      <MenuItem value="Seminar 1">Seminar 1</MenuItem>
                      <MenuItem value="Seminar 2">Seminar 2</MenuItem>
                      <MenuItem value="Seminar 3">Seminar 3</MenuItem>
                    </Select>
                  </FormControl>

                  <Box mt={2}>
                    <InputLabel style={{ color: '#FE3600' }}>Upload Cover Photo</InputLabel>
                    <Input
                      type="file"
                      fullWidth
                      onChange={handleCoverPhotoChange}
                      inputProps={{
                        accept: "image/*",
                      }}
                    />
                  </Box>
                  <Box mt={2}>
                    <InputLabel style={{ color: '#FE3600' }}>Upload More Photos (max 8)</InputLabel>
                    <Input
                      type="file"
                      fullWidth
                      inputProps={{
                        multiple: true,
                        accept: "image/*",
                      }}
                      onChange={handleMorePhotosChange}
                    />
                  </Box>
                  <Box mt={2} display="flex" justifyContent="flex-end">
                    <Button type="submit" variant="contained" sx={{ backgroundColor: '#FE3600', color: '#fff' }}
                      disabled={!isFormValid()}
                    >
                      Add News
                    </Button>
                  </Box>
                </Box>
              </Box>
            ) : (
              <Box backgroundColor={backgroundColor} p="10px" borderRadius="4px">
                <Typography variant="h5">News List</Typography>
                <List>
                  {sortedNews.map((news) => (
                    <ListItem key={news._id}>
                      <ListItemText
                        primary={news.title}
                        secondary={`Date: ${formatDate(news.date)}`}
                        sx={{
                          whiteSpace: 'pre-line',
                        }}
                      />
                      <ListItemSecondaryAction>
                        <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteNews(news._id)}>
                          <DeleteIcon />
                        </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  ))}
                </List>
              </Box>
            )}
          </Box>
        </Box>
      </Box>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        transition={Bounce}
      />
    </>
  );
};

export default News;
