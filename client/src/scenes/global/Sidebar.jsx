import { useState } from "react";
import { Sidebar, Menu, MenuItem } from "react-pro-sidebar";
import { Box, Typography, useTheme } from "@mui/material";
import { Link } from "react-router-dom";
import { tokens } from "../../theme";
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import BookingIcon from "@mui/icons-material/BookmarkAdd";
import CompletedIcon from "@mui/icons-material/Beenhere";
import ReceiptOutlinedIcon from "@mui/icons-material/ReceiptOutlined";
import ApprovedIcon from "@mui/icons-material/BookmarkAdded";
import CancelledIcon from "@mui/icons-material/BookmarkRemove";
import RecordsIcon from "@mui/icons-material/AccountBalance";
import ProfileIcon from "@mui/icons-material/AccountCircle";
import LogoutIcon from "@mui/icons-material/Logout";
import { useNavigate } from "react-router-dom";

const SidebarItem = ({ title, to, icon, selected, setSelected }) => {
  const theme = useTheme();
  const colors = tokens(theme.palette.mode);

  const activeStyles = {
    color: "#6870fa",
  };

  return (
    <MenuItem
      active={selected === title}
      style={{ color: colors.grey[100] }}
      onClick={() => setSelected(title)}
      icon={icon}
      component={<Link to={to} />}
      sx={{
        '&.ps-menuitem-root-active': activeStyles,
      }}
    >
      <Typography style={selected === title ? activeStyles : {}}>
        {title}
      </Typography>
    </MenuItem>
  );
};

const CustomSidebar = () => {

  const navigate = useNavigate();

  const theme = useTheme();
  const colors = tokens(theme.palette.mode);
  const [selected, setSelected] = useState("Dashboard");

  const handleLogout = () => {
    console.log("hello")
    navigate("/");
  }

  const sidebarStyles = {
    container: {
      "& .ps-sidebar-container": {
        background: `${colors.primary[400]} !important`,
      },
      "& .ps-menuitem-root": {
        padding: "5px 35px 5px 20px !important",
        "&:hover": {
          backgroundColor: "transparent !important",
        },
      },
      "& .ps-menuitem-root-active": {
        color: "#6870fa !important",
      },
    },
    logoBox: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      marginLeft: "15px",
    },
    profileImage: {
      cursor: "pointer",
    },
    profileBox: {
      textAlign: "center",
    },
  };

  return (
    <Box sx={sidebarStyles.container}>
      <Sidebar>
        <Menu iconShape="square">
          <Box mb="25px">
            <Box display="flex" justifyContent="center" alignItems="center">
              <img
                alt="profile-user"
                width="100px"
                height="100px"
                src={require("../../Img/Logo.svg").default} // Use require for local images
                style={sidebarStyles.profileImage}
              />
            </Box>
            <Box sx={sidebarStyles.profileBox}>
              <Typography
                variant="h2"
                color={colors.grey[100]}
                fontWeight="bold"
                sx={{ m: "10px 0 0 0" }}
              >
                Dungkar
              </Typography>
              <Typography variant="h3" color={colors.grey[100]}>
                Dzong
              </Typography>
            </Box>
          </Box>

          <Box paddingLeft="10%">
            <SidebarItem
              title="Dashboard"
              to="/dashboard"
              icon={<HomeOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="News"
              to="/news"
              icon={<ReceiptOutlinedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="Booking"
              to="/booking"
              icon={<BookingIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="Completed"
              to="/completed"
              icon={<CompletedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="Approved"
              to="/approved"
              icon={<ApprovedIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="Cancelled"
              to="/cancelled"
              icon={<CancelledIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="Records"
              to="/records"
              icon={<RecordsIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            <SidebarItem
              title="Profile"
              to="/profile"
              icon={<ProfileIcon />}
              selected={selected}
              setSelected={setSelected}
            />
            {/* <SidebarItem
              title="Logout"
              to="/"
              icon={<LogoutIcon />}
              selected={selected}
              setSelected={setSelected}
              onClick={handleLogout}
            /> */}
            <div style={{display:"flex",justifyContent:"center",marginBottom:"10px"}}>
              <button style={{padding:"10px",cursor:"pointer"}}
                onClick={handleLogout}
              >
                Logout
              </button>
            </div>
          </Box>
        </Menu>
      </Sidebar>
    </Box>
  );
};

export default CustomSidebar;
